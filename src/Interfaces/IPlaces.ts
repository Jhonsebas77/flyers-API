export interface IPlaces {
    nameCity: string,
    nameCountry: string,
    nameAirport: string,
    iata: string
}